<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'greenwood_cemetery' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':pUA|X[_DW?2Lu#*Ia6glW/7sS8F {mGP($&P20=hD|K=w0|3G#3G_l,[ue_O5<&' );
define( 'SECURE_AUTH_KEY',  ',Uf&!:8SOyCw.x<Rf.<s4Q;:zP};f,i L B>]dV~k|N V{/}a1OT}s|+@ w)?j(?' );
define( 'LOGGED_IN_KEY',    '#!@-/q}bHZf&9oXa<Cq:%ClS!VwX3|!+P%l67S*x={lZ]SDDiyraq|>|mtO9!&H{' );
define( 'NONCE_KEY',        'AU}]]WFFK$d@g]~f>Ga>tmote##~`~WkXy+:<KK;~42ZT}Da)F*_Q!${Gp2uXU|k' );
define( 'AUTH_SALT',        'z(]&@8CL}uAIOh}!B[^zrEN=V]^7psOfrU=e3VD2A<J4_4iDg%6r@<h},}Txz~Bp' );
define( 'SECURE_AUTH_SALT', 'bzOVSiM2}{ O@b7W-[GRN=(X&JbHt{0#vIRA7Yg0&/X(q[366`Bn@(g`uq,^;qYT' );
define( 'LOGGED_IN_SALT',   '$,^2j_Y]>t|/tQ2IFMI,BI6>;y`4sS6S1eP:I$XV!(M>;(`P63v5S@6s)F!BtP4:' );
define( 'NONCE_SALT',       '/o_t`-Wajp)^Loet0AT%kRq7((q).??=buUzW71u!Pff%$jSZk~p^wNY)c+R9phq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
