<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Greenwood_Cemetery
 */

?>

<footer id="colophon" class="site-footer">
    <section class="site--footer" style="background-color:#cbcebc;">
        <div class="container py-1 py-md-3">
            <div class="row text-center">
                <div class="col-12 site-info">
                        <?php
                        /* translators: %s: CMS name, i.e. WordPress. */
                        printf(esc_html__('Web development and design by %s', 'greenwood-cemetery'), 'diego.juniper creative, llc.');
                        ?>
                        <span class="sep"> | </span>
                        <?php
                        /* translators: 1: Theme name, 2: Theme author. */
                        printf(__('All Rights Reserved.'));
                        ?>
                </div><!-- .site-info -->
            </div>
        </div>
    </section>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>