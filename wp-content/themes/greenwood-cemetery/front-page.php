<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Greenwood_Cemetery
 */

get_header();
?>

	<main id="primary" class="site-main">

    <?php get_template_part( 'template-parts/content', 'hero' );?>
    <div class="container my-4">
        <div class="row text-center justify-content-center">
            <div class="card col-12 col-md-3 p-4 mx-2">
                <h3>Promise and Progress</h3>
                <p>Click here to check out our Events Blog. You can learn about the restoration progress at Historic Greenwood Cemetery. </p>
                <a href="#" class="btn btn-primary">Read Our Blog</a>
            </div>
            <div class="card col-12 col-md-3 p-4 mx-2">
                <h3>Ways You Can Help</h3>
                <ul class="text-left">
                    <li>Volunteer</li>
                    <li>GoFundMe</li>
                    <li>PayPal</li>
                    <li>Check or Money Order</li>
                </ul>
                <a href="/other-ways-to-give-to-greenwood" class="btn btn-primary">Support Greenwood</a>
            </div>
            <div class="card col-12 col-md-3 p-4 mx-2">
                <h3>Stay Connected</h3>
                <p>Join our mailing list and check out our FaceBook page/discussion group.</p>
                <a href="#" class="btn btn-primary">Join Our Mailing List</a>
            </div>
        </div>
    </div>
		<?php
		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
