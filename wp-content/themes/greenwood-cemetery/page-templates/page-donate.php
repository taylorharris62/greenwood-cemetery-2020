<?php

/**
 * Template Name: Donate Page
 *
 * @package DJuniper
 * @subpackage Greenwood Cemetery
 * @since Greenwood Cemetery 1.0
 */

get_header();
?>
<?php //get_template_part( 'template-parts/content', 'hero' );
?>

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="card col-12 col-md-6 mb-5">
                <h2 class="p-2 text-center">On-Line Donation</h2>
                <p class="p-2">There are two safe, secure, and convenient ways you can make an online tax-deductible donation to help restore Historic Greenwood Cemetery.
                    You can make a donation to our GoFundMe page located at: https://www.gofundme.com/historicgreenwood,
                    or you can donate via PayPal by clicking the donate button below.

                    All donations to Greenwood Cemetery Preservation Association are tax deductible and will go towards maintenance, restoration, and equipment costs.</p>
                    <footer class="text-center">
                        <a href="" class="btn btn-primary">GoFundMe</a>
                        <a href="" class="btn btn-primary">PayPal</a>
                    </footer>
            </div>
            <div class="col-12 col-md-6">
                <h2>Amazon Smile</h2>
                <p>You can donate to Greenwood Cemetery Preservation association just by shopping at Amazon.com.
                </p>
            </div>
            <div class="col-12 col-md-6">
                <h2>Check or Money Order</h2>
                <p>
                Send a check or money order made out to Greenwood Cemetery Preservation Association to:
GCPA
​P.O. Box 266
Florissant, MO 63032
                </p>
            </div>
            <div class="col-12 col-md-6">
                <h2>Volunteer</h2>
                <p>Volunteers are the lifeblood of Greenwood Cemetery. It is only with the generosity and effort given by volunteers that we've been able to accomplish so much over the past few months. If you'd like more information about volunteering or would like to join our volunteer mailing list, please click the button below.</p>
                <a href="" class="btn btn-primary">Click Here for Volunteer Info</a>
            </div>
            <div class="col-12 col-md-6">
                <h2>Estate Gifts</h2>
                <p>If you'd like more information about charitable estate gifts, send us an email at <a href="mailto:giving@greenwoodstl.org">giving@greenwoodstl.org</a>.
                </p>
            </div>
        </div>
    </div>
</section>


<?php
get_footer();
