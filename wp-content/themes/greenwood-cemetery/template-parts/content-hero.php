<?php

/**
 * Template part for displaying the Hero
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Greenwood_Cemetery
 */

?>

<section class="section--hero" style="min-height: 40vw;background-repeat: no-repeat; background-size: cover; background-image:url(<?php echo wp_get_attachment_image_url(get_field('background_image'), 'full'); ?>);">
    <div class="container-fluid py-md-5" style="border: 1px solid purple;">
        <div class="row py-md-5 text-center">
            <div class="col-12">
                <h1>Greenwood Cemetery Preservation Association</h1>
                <a href="#!" class="btn btn-primary">Learn More</a>
            </div>
        </div>
    </div>

</section>